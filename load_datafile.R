load_datafile <- function(filename, data_start_row = 8, data_start_col = 12, phenotype_indices = 2:6, phenotype_names = c('stage', '???_1', '???_2', 'alive', 'type')) {

data_file <- read.csv(filename);

# creating an ExpressionSet object

assay_data <- apply(data_file[data_start_row:nrow(data_file), data_start_col:(ncol(data_file))], 2, as.numeric);
#colnames(assay_data) <- as.character(data_file[7, 12:(ncol(data_file))]);
rownames(assay_data) <- as.character(data_file[data_start_row:nrow(data_file), 1]);
# TODO: arguments for this stuff
p_data <- data.frame(t(data_file[phenotype_indices, data_start_col:(ncol(data_file))]));
colnames(p_data) <- phenotype_names;
metadata <- data.frame(labelDescription = colnames(p_data),
                       row.names = colnames(p_data));
                       pheno_data <- new('AnnotatedDataFrame', data = p_data, varMetadata = metadata);

# annotation data
annotation_data <- data_file[data_start_row:nrow(data_file),1:(data_start_col - 1)];
rownames(annotation_data) <- as.character(data_file[data_start_row:nrow(data_file),1]);
colnames(annotation_data) <- as.character(data_file[data_start_row - 1,1:(data_start_col - 1)]);
annotation_metadata = data.frame(labelDescription = colnames(annotation_data), row.names = colnames(annotation_data));
anno_data <- new('AnnotatedDataFrame', data = annotation_data, varMetadata = annotation_metadata);

expSet = ExpressionSet(assayData = assay_data,
    phenoData = pheno_data,
    featureData = anno_data);

return(expSet);

}
