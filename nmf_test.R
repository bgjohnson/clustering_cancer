library(NMF);
library(golubEsets);

data(Golub_Train);
data(Golub_Test);
data(Golub_Merge);


golub_merge <- data.frame(Golub_Merge)[1:7129];
golub_test <- data.frame(Golub_Test)[1:7129];
golub_train <- data.frame(Golub_Train)[1:7129];
# use Brunet's algorithm?

data(esGolub);

meth <- nmfAlgorithm(version = "R")
meth <- c(names(meth), meth)

res <- nmf(esGolub, 3, 'brunet', seed = 123456, nrun = 1);

# methods to get data from a NMF result object
fit(res);
fitted(res);
summary(res);
summary(res, target = esGolub);
# the basis matrix is W - the feature-class matrix or the "metagene" matrix
W <- basis(res);
# the coef matrix is H - the class-sample matrix or the metagene expression profiles
H <- coef(res);

s <- featureScore(res);

# characterizes each metagene
s1 <- extractFeatures(res);

# making a "consensus plot"
png('golub_nmf_brunet_consensus.png', res = 240, width=8, height=8, units='in');
consensusmap(res, annCol = esGolub);
dev.off();
